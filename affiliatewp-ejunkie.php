<?php
/**
 * Plugin Name: AffiliateWP: Migrate from E-Junkie
 * Plugin URI: http://katz.co
 * Description: Import affiliates from E-Junkie into AffiliateWP.
 * Author: Katz Web Services, Inc.
 * Author URI: http://katz.co
 * Version: 1.1.2
 * Text Domain: affiliatewp-ejunkie-migrate
 * Domain Path: languages
 *
 * @todo Implement logging class that hooks into `ejunkie_affiliatewp_import_debug`
 * @todo Add auto-update using EDD License class
 */

class E_Junkie_AffiliateWP_Import {

	const version = '1.1.2';

	/**
	 * Name we expect hosting website to have.
	 */
	const name = 'AffiliateWP: Migrate from E-Junkie';

	const slug = 'ejunkie-migrate-to-affiliatewp';

	/**
	 * Plugin base file __FILE__
	 * @var string
	 */
	static $file;

	/**
	 * Include path to plugin base file.
	 * @var string
	 */
	static $path;

	/**
	 * Key used for WP_Error objects
	 * @var string
	 */
	var $error_key = 'ejunkie-affiliatewp-import';

	/**
	 * Holder of processing notices
	 * @var array
	 */
	var $notices = array();

	/**
	 * Holder of processing errors
	 * @var array
	 */
	var $errors = array();

	/**
	 * Counter for how many rows have been processed.
	 * @var integer
	 */
	var $processed = 0;

	/**
	 * Counter for how many users have been created.
	 * @var integer
	 */
	var $added_users = 0;

	/**
	 * Array of added affiliate data, with each key being the affiliate ID.
	 * @var array
	 */
	var $inserted_affiliates = array();

	function __construct() {

		self::$file = __FILE__;

		self::$path = plugin_dir_path( __FILE__ );

		require_once( self::$path . 'class.license.php' );

		// Display the import e-junkie affiliates metabox
		add_action( 'affwp_tools_tab_migration', array( &$this, 'import_ejunkie_affiliates_metabox') );

		// Verify and capture the submitted form data
		add_action( 'affwp_import_affiliates_from_ejunkie', array( &$this, 'handle_upload'));

		// Process the contents of the submitted file. Triggered by handle_upload
		add_action('ejunkie_affiliatewp_process_import', array( &$this, 'process_upload'), 10 );

		// Handle notifications, reporting
		add_action('ejunkie_affiliatewp_process_import', array( &$this, 'after_import'), 1000 );

		// Display any errors or updates thrown by the processing
		add_action('admin_notices', array( &$this, 'admin_notices' ) );

	}

	/**
	 * Handle notifications, reporting.
	 * @return void
	 */
	function after_import() {

		$output = '';

		$output .= sprintf( __( '%d rows were processed', 'affiliatewp-ejunkie-migrate' ), $this->processed );

		if( empty( $this->inserted_affiliates ) ) {
			$output .= sprintf( __( ', but no affiliates were added. Maybe they were already affiliates', 'affiliatewp-ejunkie-migrate' ), $this->processed );
		} else {
			$output .= sprintf( __( '; %d %s added', 'affiliatewp-ejunkie-migrate' ), sizeof( $this->inserted_affiliates ), _n( 'affiliate was', 'affiliates were', sizeof( $this->inserted_affiliates ), 'affiliatewp-ejunkie-migrate') );
		}

		if( !empty( $this->added_users ) ) {
			$output .= sprintf( __( ' and %d %s created', 'affiliatewp-ejunkie-migrate' ), $this->added_users, _n( 'user was', 'users were', $this->added_users, 'affiliatewp-ejunkie-migrate')  );
		}

		// Set the header for the rest of the notice box.
		$this->notices[] = '<h3>'.$output.'.</h3>';

		if( !empty( $this->inserted_affiliates ) ) {

			$output = '<ul class="ul-disc">';

			foreach ( (array) $this->inserted_affiliates as $affiliate_id => $user_data ) {

				$output .= '<li>'.sprintf( 'Added Affiliate #%d (%s)', $affiliate_id, $user_data['email'] ).'</li>';

				$this->maybe_notify_affiliate( $affiliate_id );
			}

			$output .= '</ul>';

			$this->notices[] = $output;
		}
	}

	/**
	 * Maybe notify affiliates after import
	 *
	 * @since 1.1.2
	 * @param int $affiliate_id Imported Affiliate ID
	 * @filter affiliatewp_ejunkie_notify_imported_affiliates boolean: Whether to notify imported affiliates. Default: `false`
	 * @filter affiliatewp_ejunkie_import_status Change whether affiliate should be 'active', 'pending', or 'rejected' (Default: `active`)
	 * @return void
	 */
	function maybe_notify_affiliate( $affiliate_id ) {

		// Should imported affiliates be emailed?
		$notify = apply_filters( 'affiliatewp_ejunkie_notify_imported_affiliates', false );

		$status = apply_filters( 'affiliatewp_ejunkie_import_status', 'active' );

		if( $notify ) {
			switch ( $status ) {
				case 'active':
					if( function_exists( 'affwp_notify_on_approval' ) ) {
						affwp_notify_on_approval( $affiliate_id, $status );
					}
					break;
				case 'pending':
					if( function_exists( 'affwp_notify_on_pending_affiliate_registration' ) ) {
						affwp_notify_on_pending_affiliate_registration( $affiliate_id, $status );
					}
					break;
				case 'rejected':
					if( function_exists( 'affwp_notify_on_rejected_affiliate_registration' ) ) {
						affwp_notify_on_rejected_affiliate_registration( $affiliate_id, $status, 'pending' );
					}
					break;
			}
		}
	}

	/**
	 * Add Import Metabox to AffiliateWP Import/Export page
	 *
	 * @since       1.0
	 * @return      void
	 */
	function import_ejunkie_affiliates_metabox() {

	?>
		<div id="affwp-e-junkie-dashboard-widgets-wrap">
			<div class="metabox-holder">
				<div class="postbox">
					<h3><span><?php _e( 'E-Junkie - Import Affiliates', 'affiliate-wp' ); ?></span></h3>
					<div class="inside">

						<p><img src="<?php echo plugins_url('assets/images/e-junkielogo.png', __FILE__ ); ?>" class="alignright" alt="" /><?php _e( 'Import affiliates from an E-Junkie export file generated by E-Junkie.'); ?></p>

						<h4><?php esc_html_e( 'To generate the download file:', 'affiliatewp-ejunkie-migrate' ); ?></h4>

						<ul class="ul-disc">
							<li><?php esc_html_e( 'Visit Seller Admin > Manage Affiliates > View/Edit Affiliates.', 'affiliatewp-ejunkie-migrate' ); ?></li>
							<li><?php esc_html_e( 'Click the "Download List" button to generate the import file.', 'affiliatewp-ejunkie-migrate' ); ?></li>
							<li><?php esc_html_e( 'If the downloaded file is in .zip format, double-click the file to expand.', 'affiliatewp-ejunkie-migrate' ); ?></li>
							<li><?php esc_html_e( 'Select the .txt file shown (may be in a folder named "dl")', 'affiliatewp-ejunkie-migrate' ); ?></li>
						</ul>

						<form method="post" enctype="multipart/form-data" action="<?php echo admin_url( 'admin.php?page=affiliate-wp-tools&amp;tab=migration' ); ?>">
							<p>
								<input type="file" name="edd-ejunkie-tsv" />
							</p>
							<p>
								<input type="hidden" name="affwp_action" value="import_affiliates_from_ejunkie" />
								<?php wp_nonce_field('edd-ejunkie-import-upload', 'edd-ejunkie-import-upload'); ?>
								<?php submit_button( __( 'Import Affiliates from E-Junkie', 'affiliatewp-ejunkie-migrate' ), 'secondary', 'submit', false ); ?>
							</p>
						</form>
					</div><!-- .inside -->
				</div><!-- .postbox -->
			</div><!-- .metabox-holder -->
		</div><!-- #affwp-dashboard-widgets-wrap -->
	<?php
	}

	/**
	 * Display any errors or notices that were triggered during import.
	 *
	 * @since       1.0
	 * @return      void
	 */
	function admin_notices() {

		if( empty($this->errors) && empty($this->notices) ) { return; }

		if( !empty($this->errors) ) {
			foreach ( (array)$this->errors as $key => $error) {
				add_settings_error( $this->error_key, $this->error_key, wpautop( $error->get_error_message() ), 'error' );
			}
		}

		if( !empty( $this->notices ) ) {

			foreach ( (array)$this->notices as $key => $notice) {
				$output .= wpautop( $notice );
			}

			add_settings_error( $this->error_key, $this->error_key, $output, 'updated' );
		}

		settings_errors( $this->error_key );

	}

	/**
	 * Process the submission from the Import metabox form
	 *
	 * @return void
	 */
	function handle_upload() {

		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		if( empty( $_POST['edd-ejunkie-import-upload'] ) ) {
			do_action('ejunkie_affiliatewp_import_debug', __('Somehow a request was submitted without having the `edd-ejunkie-import-upload` $_POST set.', 'affiliatewp-ejunkie-migrate'), 'handle_upload' );
			return false;
		}

		if( ! wp_verify_nonce( $_POST['edd-ejunkie-import-upload'], 'edd-ejunkie-import-upload' ) ) {
			$this->errors[] = new WP_Error( $this->error_key, sprintf( __('Your request was invalid. Try %sclicking here to refresh the page%s and then try uploading the file again.', 'affiliatewp-ejunkie-migrate'), '<a href="'.admin_url('admin.php?page=affiliate-wp-tools&amp;tab=export_import' ).'">', '</a>' ) );
			do_action('ejunkie_affiliatewp_import_debug', __('Nonce is invalid.', 'affiliatewp-ejunkie-migrate'), 'handle_upload' );
			return false;
		}

		if( ! current_user_can( 'manage_options' ) ) {
			do_action('ejunkie_affiliatewp_import_debug', __('User does not have `manage_options` capabilities.', 'affiliatewp-ejunkie-migrate'), 'handle_upload' );
			return false;
		}


		// Upload the files using WordPressy ways.
		// We do this for server compat, security, and to allow WP to determine file type.
		$uploadedfile = $_FILES['edd-ejunkie-tsv'];
		$upload_overrides = array( 'test_form' => false );
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

		// It worked
		if ( $movefile ) {

			// Except it threw an error
			if( !empty( $movefile['error'] ) ) {
				$this->errors[] = new WP_Error( $this->error_key, $movefile['error'] );
				do_action('ejunkie_affiliatewp_import_debug', sprintf( __("There was an error after processing upload with `wp_handle_upload`: %s", 'affiliatewp-ejunkie-migrate'), $movefile['error'] ), 'handle_upload' );
				return false;
			}

			// Let's make sure the file type is correct
		    switch( $movefile['type'] ) {

		    	case 'text/tab-separated-values':
		    	case 'text/plain':

		    		do_action('ejunkie_affiliatewp_import_debug', __("File is valid type, and was successfully uploaded.", 'affiliatewp-ejunkie-migrate'), 'handle_upload' );

		    		// get file contents
		    		$file_contents = file_get_contents( $movefile[ 'file' ] );

		    		do_action( 'ejunkie_affiliatewp_process_import', $file_contents );

		    		// Remove the uploaded file
		    		unlink( $movefile['file'] );

		    		break;

		    	default:

		    		$this->errors[] = new WP_Error( $this->error_key, __( 'The file is not a valid type. The file exported by E-Junkie should be in .txt format.', 'affiliatewp-ejunkie-migrate' ) );

		    		// Remove the uploaded file
		    		unlink( $movefile['file'] );

		    		return false;
		    		break;
		    }


		} else {
			$error_message = __( 'wp_handle_upload failed. This shouldn\'t happen, and may signal an upload attack.', 'affiliatewp-ejunkie-migrate' );
			$this->errors[] = new WP_Error( $this->error_key, $error_message );
			do_action('ejunkie_affiliatewp_import_debug', $error_message, 'handle_upload' );
			return false;
		}
	}

	/**
	 * Process the contents of the submitted file. Triggered by `handle_upload`
	 *
	 * @param  string      $file_contents The contents of the uploaded file.
	 * @return void
	 */
	function process_upload( $file_contents ) {

		// What just happened?
		if( !is_string( $file_contents ) ) {

			$error_message = __( 'The uploaded file was not processed because its contents were not read correctly.', 'affiliatewp-ejunkie-migrate' );

			$this->errors[] = new WP_Error( 'upload-not-processed', $error_message );

			do_action('ejunkie_affiliatewp_import_debug', $error_message, 'process_upload' );

			return;
		}

		// Each new line is an affiliate
		$lines = explode("\n", $file_contents);

		/**
		 * Each line contains these columns, tsv:
		 * Array
		 *	(
		 *	    [0] => Email
		 *	    [1] => PayPal Email
		 *	    [2] => Date of Creation
		 *	    [3] => Extra Percentage
		 *	)
		 */
		foreach ($lines as $line => $line_content) {

			$user_data = array();

			// Explode the line into an associative array with nice keys.
			list( $user_data['email'], $user_data['paypal_email'], $user_data['creation_date'], $user_data['extra_percentage'] ) = explode("\t", $line_content);

			// This is the header line. Keep going.
			if( $line === 0 && !is_email( $user_data['email'] ) ) {
				do_action('ejunkie_affiliatewp_import_debug', __( 'Skipping header line.', 'affiliatewp-ejunkie-migrate' ), 'process_upload' );
				continue;
			} else if( !is_email( $user_data['email'] ) ) {
				$this->errors[] = new WP_Error( 'invalid-email', sprintf( __('%s is an invalid email address.', 'affiliatewp-ejunkie-migrate' ), $user_data['email'] ) );

				do_action('ejunkie_affiliatewp_import_debug', sprintf( __( 'Invalid email: %s - skipping entry.', 'affiliatewp-ejunkie-migrate' ), $user_data['email'] ), 'process_upload' );

				continue;
			}

			$this->processed++;

			// Standardize the values.
			$user_data['extra_percentage'] = floatval( $user_data['extra_percentage'] );

			// If the payment email isn't an email, that won't do at all.
			$user_data['paypal_email'] = is_email( $user_data['paypal_email'] ) ? $user_data['paypal_email'] : NULL;

			// Does the user exist for their main email?
			$user = get_user_by( 'email' , $user_data['email'] );

			// Does the user exist for their PayPal email?
			if( !$user ) {

				do_action('ejunkie_affiliatewp_import_debug', sprintf( __( 'No user with email matching %s. Checking PayPal email.', 'affiliatewp-ejunkie-migrate' ), $user_data['email'] ), 'process_upload' );

				$user = get_user_by( 'email' , $user_data['paypal_email'] );
			}

			// The user does NOT exist. We need to create an user
			if( !$user ) {

				do_action('ejunkie_affiliatewp_import_debug', sprintf( __( 'No user with emails matching %s or %s. Creating user.', 'affiliatewp-ejunkie-migrate' ), $user_data['email'], $user_data['paypal_email'] ), 'process_upload' );

				$args = array(
					'user_login'   => sanitize_text_field( $user_data['email'] ),
					'user_email'   => sanitize_text_field( $user_data['email'] ),
					'user_pass'	   => NULL, // Fix undefined index issue with wp_insert_user()
				);

				// Register a new user with the user data
				$user_id = wp_insert_user( $args );

				// $user_id should always be an INT
				if( is_wp_error( $user_id ) ) {
					$this->errors[] = $user_id;
					continue;
				}

				// Count how many users were added.
				$this->added_users++;

				// Creating the user worked; load up the data on that just-created user.
				$user = get_userdata( $user_id );
			}

			// We have our user.
			// Now we process whether the user is an affiliate or not,
			// and if not, we create the affiliate.
			$affiliate_id = $this->maybe_insert_affiliate( $user->ID, $user_data );
		}
	}

	/**
	 * Insert a new affiliate, if not already affiliate. If already, return affiliate ID.
	 *
	 * We use this instead of `affwp_add_affiliate()` because we want to add `date_registered` values
	 * and also we don't want to trigger redirections in that function caused by `$_POST['affwp_action']` being set.
	 * @param  int      $user_id   User ID
	 * @param  array      $user_data Data to be added to the affiliate
	 * @return int|boolean                 Affiliate ID, if successful. `false` if not.
	 * @filter affiliatewp_ejunkie_affiliate_data Modify the data of the affiliate before they get added to AffiliateWP
	 * @filter affiliatewp_ejunkie_import_status Change whether affiliate should be 'active', 'pending', or 'rejected' (Default: 'active')
	 */
	function maybe_insert_affiliate( $user_id, $user_data ) {

		do_action('ejunkie_affiliatewp_import_debug', sprintf( __( 'Processing %d', 'affiliatewp-ejunkie-migrate' ), $user_id), 'maybe_insert_affiliate' );

		// If they're already an affiliate, return the ID.
		if( $affiliate_id = affwp_get_affiliate_id( $user_id ) ) {

			do_action('ejunkie_affiliatewp_import_debug', sprintf( __( 'User ID #%d is already an affiliate.', 'affiliatewp-ejunkie-migrate' ), $user_id), 'maybe_insert_affiliate' );

			return $affiliate_id;
		}

		// Calculate the rate based on default rate + extra % from E-Junkie
		if( !empty( $user_data['extra_percentage'] ) ) {

			// default rate
			$default_rate = affiliate_wp()->settings->get( 'referral_rate', 20 );

			// The user's rate is the default rate, plus the extra percentage.
			$user_data['rate'] = ( floatval( $default_rate ) + floatval( $user_data['extra_percentage'] ) );
		}

		$affiliate_data = apply_filters( 'affiliatewp_ejunkie_affiliate_data', array(
			'status'        => apply_filters( 'affiliatewp_ejunkie_import_status', 'active' ),
			'user_id'       => $user_id,
			'rate'    => ! empty( $user_data['rate'] ) ? sanitize_text_field( $user_data['rate'] ) : '',
			'payment_email' => !empty( $user_data['paypal_email'] ) ? sanitize_text_field( $user_data['paypal_email'] ) : ''
		));

		if( $affiliate_id = affiliate_wp()->affiliates->add( $affiliate_data ) ) {

			// Store what affiliates have been added
			$this->inserted_affiliates[ $affiliate_id ] = $user_data;

			do_action('ejunkie_affiliatewp_import_debug', sprintf( __( 'User ID #%d added as Affiliate #%d.' , 'affiliatewp-ejunkie-migrate' ), $user_id, $affiliate_id ), 'maybe_insert_affiliate' );

			return $affiliate_id;

		}

		$error_message = sprintf( __( 'Error adding User ID #%d as an affiliate.', 'affiliatewp-ejunkie-migrate' ), $user_id);

		$this->errors[] = new WP_Error( $this->error_key, $error_message );

		do_action('ejunkie_affiliatewp_import_debug', $error_message, 'maybe_insert_affiliate' );

		return false;
	}

}

new E_Junkie_AffiliateWP_Import;
