<?php

class E_Junkie_AffiliateWP_Import_License {

	const key = 'ejunkie_affiliate_wp_license';

	function __construct() {

		add_filter( 'affwp_settings_general', array(&$this, 'add_setting' ));

		// EDD SL plugin updater
		add_action( 'admin_init', array(&$this, 'updater') );
		add_action('admin_init', array(&$this, 'activate_license') );
		add_action('admin_init', array(&$this, 'deactivate_license') );
		add_action( 'admin_init', array(&$this, 'check_license' ) );
	}

	/**
	 * Plugin Updater
	 *
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	function updater() {

		if( ! is_admin() ) { return; }

		if( !class_exists( 'AFFWP_Plugin_Updater' ) ) {
			require_once AFFILIATEWP_PLUGIN_DIR . 'includes/admin/AFFWP_Plugin_Updater.php';
		}

		$license_key = $this->get_license_key();

		if( $license_key ) {

			$affwp_updater = new AFFWP_Plugin_Updater( 'https://katz.co', E_Junkie_AffiliateWP_Import::$file , array(
				'version' 	=> E_Junkie_AffiliateWP_Import::version,
				'license' 	=> $license_key,
				'item_name' => E_Junkie_AffiliateWP_Import::name,
				'author' 	=> 'Katz Web Services, Inc.',
			));
		}
	}

	/**
	 * Add E-Junkie Migrate Addon License Key setting to AffWP settings
	 * @param  array      $general_settings General Settings array
	 * @return  array      Settings array with the added setting placed after the AffWP setting
	 */
	function add_setting( $general_settings ) {

		$ejunkie_array_item = array(
			'ejunkie_license_key' => array(
				'name' => __( 'E-Junkie Migrate Addon License Key', 'affiliatewp-ejunkie-migrate' ),
				'desc' => $this->license_button().'<br />'.sprintf( __( 'Please enter your license key. An active license key is needed for automatic plugin updates and support.' ) ),
				'type' => 'text'
			)
		);

		$first_array = array_splice( $general_settings, 0, 2 );

		return array_merge( $first_array, $ejunkie_array_item, $general_settings );
	}

	/**
	 * Generate the activate/renew button based on the status of the license
	 * @return string      HTML output
	 */
	function license_button( ) {
		$html = '';

		$license_status = get_option( 'ejunkie_affiliatewp_license_status' );

		$license_key = $this->get_license_key();

		if( 'valid' === $license_status && ! empty( $license_key ) ) {
			$html .= '<input type="submit" class="button" name="affwp_ejunkie_deactivate_license" value="' . esc_attr__( 'Deactivate License', 'affiliatewp-ejunkie-migrate' ) . '"/>';
			$html .= '<span style="color:green;">&nbsp;' . __( 'Your license is valid!', 'affiliatewp-ejunkie-migrate' ) . '</span>';
		} elseif( 'expired' === $license_status && ! empty( $license_key ) ) {
			$renewal_url = add_query_arg( array( 'edd_license_key' => $license_key, 'download_id' => 7296 ), 'https://katz.co/checkout/' );
			$html .= '<a href="' . esc_url( $renewal_url ) . '" class="button-primary">' . __( 'Renew Your License', 'affiliatewp-ejunkie-migrate' ) . '</a>';
			$html .= '<br/><span style="color:red;">&nbsp;' . __( 'Your license has expired, renew today to continue getting updates and support!', 'affiliatewp-ejunkie-migrate' ) . '</span>';
		} else {
			$html .= '<input type="submit" class="button" name="affwp_ejunkie_activate_license" value="' . esc_attr__( 'Activate License', 'affiliatewp-ejunkie-migrate' ) . '"/>';
		}

		$html .= wp_nonce_field( 'ejunkie_affiliatewp_nonce', 'ejunkie_affiliatewp_nonce', true, false );

		return $html;
	}

	/**
	 * Check the status of the license. Happens daily.
	 * @return string      License status
	 */
	function check_license() {
		global $wp_version;

		if( ! empty( $_POST['affwp_settings'] ) ) {
			return; // Don't fire when saving settings
		}

		$status = get_transient( 'affwp_ejunkie_license_check' );

		// Run the license check a maximum of once per day
		if( false === $status ) {

			$license_data = $this->get_remote( 'check_license' );

			if ( !$license_data ) { return false; }

			$status = $license_data->license;

			set_transient( 'affwp_ejunkie_license_check', $status, DAY_IN_SECONDS );

			update_option( 'ejunkie_affiliatewp_license_status', $status );
		}

		return $status;
	}

	/**
	 * Get the license key, as saved in AffiliateWP settings
	 * @return string|null      License key, if set.
	 */
	function get_license_key() {

		// retrieve the license from the database
		$affiliatewp_settings = get_option( 'affwp_settings', array( 'ejunkie_license_key' => NULL ) );

		$license = $affiliatewp_settings['ejunkie_license_key'];

		return $license;
	}

	/**
	 * Process remote request and return license data array
	 * @param  string      $action Name of action to perform
	 * @return array              License data returned from site.
	 */
	function get_remote( $action = '' ) {

		$api_params = array(
			'edd_action'=> $action,
			'license' 	=> $this->get_license_key(),
			'item_name' => urlencode( E_Junkie_AffiliateWP_Import::name ), // the name of our product in EDD
			'slug' 		=> E_Junkie_AffiliateWP_Import::slug,
			'url'       => home_url()
		);

		// Call the custom API.
		$response = wp_remote_get( add_query_arg( $api_params, 'https://katz.co' ), array( 'timeout' => 15, 'sslverify' => false ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) ) {
			return false;
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		return $license_data;
	}

	/**
	 * Deactivate license key
	 * @return false|void Returns false if get_remote triggered wp_error
	 */
	function deactivate_license() {

		// listen for our activate button to be clicked
		if( isset( $_POST['affwp_ejunkie_deactivate_license'] ) ) {

			// run a quick security check
		 	if( ! check_admin_referer( 'ejunkie_affiliatewp_nonce', 'ejunkie_affiliatewp_nonce' ) )
				return; // get out if we didn't click the Activate button

			// retrieve the license from the database
			$license = trim( get_option( self::key ) );


			// data to send in our API request
			$license_data = $this->get_remote( 'deactivate_license' );

			if ( !$license_data ) { return false; }

			// $license_data->license will be either "deactivated" or "failed"
			if( $license_data->license == 'deactivated' ) {
				delete_option( 'ejunkie_affiliatewp_license_status' );
				delete_transient( 'affwp_ejunkie_license_check' );
			}

		}
	}

	/**
	 * Activate license key
	 * @return false|void Returns false if get_remote triggered wp_error
	 */
	function activate_license() {

		// listen for our activate button to be clicked
		if( isset( $_POST['affwp_ejunkie_activate_license'] ) ) {

			// run a quick security check
		 	if( ! check_admin_referer( 'ejunkie_affiliatewp_nonce', 'ejunkie_affiliatewp_nonce' ) )
				return; // get out if we didn't click the Activate button

			$license_data = $this->get_remote( 'activate_license' );

			if ( !$license_data ) { return false; }

			// $license_data->license will be either "valid" or "invalid"
			update_option( 'ejunkie_affiliatewp_license_status', $license_data->license );
			delete_transient( 'affwp_ejunkie_license_check' );
		}
	}

}

new E_Junkie_AffiliateWP_Import_License;