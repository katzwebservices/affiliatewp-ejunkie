=== AffiliateWP: Migrate from E-Junkie ===
Tags: affiliatewp, affiliate, ejunkie
Requires at least: 3.3
Tested up to: 4.4.2
Stable tag: trunk
Contributors: katzwebdesign, katzwebservices
License: GPL 3 or higher

Import E-Junkie Affiliates into AffiliateWP.

== Description ==

### Setting Up the plugin:

- Install the plugin
- Go to the Affiliates > Tools page
- Click on the "Migration Assistant" tab
- Follow the instructions under "Import Affiliates from E-Junkie"

== Screenshots ==


== Installation ==

1. Upload plugin files to your plugins folder, or install using WordPress' built-in Add New Plugin installer
2. Activate the plugin
3. Install the plugin
4. Go to the Affiliates > Tools page
5. Click on the "Migration Assistant" tab
6. Follow the instructions under "Import Affiliates from E-Junkie"

== Frequently Asked Questions ==

== Changelog ==

= 1.1.1 & 1.1.2 on March 25, 2016 =
* Fixed: Sending email notifications with AffiliateWP 1.7.16+
* Now requires AffiliateWP 1.7.16 or higher
* Fixed: `Call to a member function get_error_message() on string` error

= 1.1 =
* Added license functionality

= 1.0.3 =
* Removed `creation_date` parameter from Affiliate import; it is the date of registering with E-Junkie, not with the affiliate program.

= 1.0.2 = 
* Fixed undefined index issue with `wp_insert_user()`

= 1.0.1 =
* Moved metabox to Migration Assistant
* Added validation that the Payment Email is an email address

= 1.0 =

* Liftoff!

== Upgrade Notice ==

= 1.1 =
* Added license functionality

= 1.0.3 =
* Removed `creation_date` parameter from Affiliate import; it is the date of registering with E-Junkie, not with the affiliate program.

= 1.0.2 = 
* Fixed undefined index issue with `wp_insert_user()`

= 1.0.1 =
* Moved metabox to Migration Assistant
* Added validation that the Payment Email is an email address

= 1.0 =

* Liftoff!